package main

import(
	"github.com/kataras/iris"
	"./common"
	"./routes"
)

func main(){
	// init application settings
	common.Init()

	// init iris framework
	api := iris.New()

	// register routes
	routes.RegisterRoutes(api)

	// start listening application
	api.Listen(common.AppConfig.Port)
}