package event

import (
	"../../models"
	"github.com/kataras/iris"

)



func Trigger(c *iris.Context) models.EventExecutionResponse {

	data := models.EventExecutionRequest{}
	err := c.ReadJSON(&data)

	if  err != nil {
	    panic(err.Error())
	}

	response := Execute(data)
	return response
}

func Execute(event models.EventExecutionRequest) models.EventExecutionResponse{

	//providers.SendEmail()
	response := models.EventExecutionResponse{ Message:event.EventKey }

	return response
}
