package controllers

import (
	"github.com/kataras/iris"
	"../process/event"
)

func FireEvent(c *iris.Context)  {
	response:= event.Trigger(c)
	c.JSON(iris.StatusOK, response)
}